import { ActorPF } from "./actor/entity.js";

/**
 * Highlight critical success or failure on d20 rolls
 */
export const highlightCriticalSuccessFailure = function(message, html, data) {
  if ( !message.roll || !message.roll.parts.length ) return;

  // Highlight rolls where the first part is a d20 roll
  let d = message.roll.parts[0];
  const isD20Roll = d instanceof Die && (d.faces === 20) && (d.results.length === 1);
  const isModifiedRoll = (d.rolls != null && "success" in d.rolls[0]) || (d.options != null && (d.options.marginSuccess || d.options.marginFailure));
  if ( isD20Roll && !isModifiedRoll ) {
    if (d.total >= (d.options.critical || 20)) html.find(".dice-total").addClass("success");
    else if (d.total <= (d.options.fumble || 1)) html.find(".dice-total").addClass("failure");
  }
};

/* -------------------------------------------- */

/**
 * Optionally hide the display of chat card action buttons which cannot be performed by the user
 */
export const displayChatActionButtons = function(message, html, data) {
  const chatCard = html.find(".pf1.chat-card");
  if ( chatCard.length > 0 ) {

    // If the user is the message author or the actor owner, proceed
    let actor = game.actors.get(data.message.speaker.actor);
    if ( actor && actor.owner ) return;
    else if ( game.user.isGM || (data.author.id === game.user.id)) return;

    // Otherwise conceal action buttons except for saving throw
    const buttons = chatCard.find("button[data-action]");
    buttons.each((i, btn) => {
      if ( btn.dataset.action === "save" ) return;
      btn.style.display = "none"
    });
  }
};

/* -------------------------------------------- */

export const createCustomChatMessage = async function(chatTemplate, chatTemplateData={}, chatData={}) {
  let rollMode = game.settings.get("core", "rollMode");
  chatData = mergeObject({
    rollMode: rollMode,
    user: game.user._id,
    type: CONST.CHAT_MESSAGE_TYPES.CHAT,
    sound: CONFIG.sounds.dice,
    content: await renderTemplate(chatTemplate, chatTemplateData),
  }, chatData);
  // Handle different roll modes
  switch (chatData.rollMode) {
    case "gmroll":
      chatData["whisper"] = game.users.entities.filter(u => u.isGM).map(u => u._id);
      break;
    case "selfroll":
      chatData["whisper"] = [game.user._id];
      break;
    case "blindroll":
      chatData["whisper"] = game.users.entities.filter(u => u.isGM).map(u => u._id);
      chatData["blind"] = true;
  }

  ChatMessage.create(chatData);
};
